'use strict';
const gulp = require('gulp');
const shell = require('shelljs');
const size = require('gulp-size');
const argv = require('yargs').argv;
const notifier = require('node-notifier');
const path = require('path');

// 'gulp jekyll:tmp' -- copies your Jekyll site to a temporary directory
// to be processed
gulp.task('site:tmp', () =>
  gulp.src(['src/**/*', '!src/assets/**/*', '!src/assets'], {dot: true})
    .pipe(gulp.dest('.tmp/src'))
    .pipe(size({title: 'Jekyll'}))
);

// 'gulp jekyll' -- builds your site with development settings
// 'gulp jekyll --prod' -- builds your site with production settings
gulp.task('site', done => {
  let cmd;

  if (!argv.prod) {
    cmd = 'jekyll build';
  } else {
    cmd = 'jekyll build --config _config.yml,_config.prod.yml';
  }

  const resultCode = shell.exec(cmd).code;

  if (resultCode !== 0) {
    notifier.notify({
      'title': 'Error',
      'message': 'Error compilando JEKYLL, revisa la consola',
      icon: path.join(__dirname, '../../node_modules/gulp-notify/assets/gulp-error.png'),
    });
  }

  done();
});

// 'gulp doctor' -- literally just runs jekyll doctor
gulp.task('site:check', done => {
  shell.exec('jekyll doctor');
  done();
});
