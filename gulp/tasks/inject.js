'use strict';
const gulp = require('gulp');
const inject = require('gulp-inject');
const series = require('stream-series');

// 'gulp inject:head' -- injects our style.css file into the head of our HTML
gulp.task('inject:head', () =>
  gulp.src('.tmp/src/_includes/head.html')
    .pipe(inject(gulp.src('.tmp/assets/stylesheets/style.css', {read: false}), {ignorePath: '.tmp'}))
    .pipe(gulp.dest('.tmp/src/_includes'))
);

gulp.task('inject:js:head', () =>
  gulp.src('.tmp/src/_includes/head.html')
      .pipe(inject(
              gulp.src([
                '.tmp/assets/javascript/vendor/jquery.min.js',
              ], {read: false}),
              { name: 'head', ignorePath: '.tmp' }
            ))
      .pipe(gulp.dest('.tmp/src/_includes'))
);

// 'gulp inject:footer' -- injects our index.js file into the end of our HTML
gulp.task('inject:js:footer', () => {
  const destDir = '.tmp/src/_layouts';

  const vendorStream = gulp.src([
      '.tmp/assets/javascript/vendor/placeholders.jquery.js',
    ], {read: false});
  const appStream = gulp.src([
      '.tmp/assets/javascript/*.js',
      '!.tmp/assets/javascript/proto.js'
    ], {read: false});

  return gulp.src('.tmp/src/_layouts/default.html')
    .pipe(inject(series(vendorStream, appStream), { name: 'other', ignorePath: '.tmp'}))
    .pipe(gulp.dest('.tmp/src/_layouts'));
});
