'use strict';
const argv = require('yargs').argv;
const autoprefixer = require('autoprefixer');
const browserSync = require('browser-sync').create();
const concat = require('gulp-concat');
const cssnano = require('gulp-cssnano');
const gulp = require('gulp');
const gzip = require('gulp-gzip');
const newer = require('gulp-newer');
const postcss = require('gulp-postcss');
const rename = require('gulp-rename');
const rev = require('gulp-rev');
const sass = require('gulp-sass');
const size = require('gulp-size');
const sourcemaps = require('gulp-sourcemaps');
const uglify = require('gulp-uglify');
const when = require('gulp-if');
const mergeStream = require('merge-stream');
const plumber = require('gulp-plumber');
const notify = require('gulp-notify');

// 'gulp scripts' -- creates a index.js file from your JavaScript files and
// creates a Sourcemap for it
// 'gulp scripts --prod' -- creates a index.js file from your JavaScript files,
// minifies, gzips and cache busts it. Does not create a Sourcemap
gulp.task('scripts', () =>
  // NOTE: The order here is important since it's concatenated in order from
  // top to bottom, so you want vendor scripts etc on top
  gulp.src([
    'src/assets/javascript/*.js',
  ])
    .pipe(plumber({
      errorHandler: notify.onError('Error compilando JS, revisa la consola\n <%= error.message %>'),
    }))
    // .pipe(newer('.tmp/assets/javascript/index.js', {dest: '.tmp/assets/javascript', ext: '.js'}))
    // .pipe(when(!argv.prod, sourcemaps.init()))
    // .pipe(concat('index.js'))
    .pipe(size({
      showFiles: true
    }))
    .pipe(when(argv.prod, rename({suffix: '.min'})))
    .pipe(when(argv.prod, when('*.js', uglify({preserveComments: 'some'}))))
    .pipe(when(argv.prod, size({
      showFiles: true
    })))
    .pipe(when(argv.prod, rev()))
    // .pipe(when(!argv.prod, sourcemaps.write('.')))
    .pipe(when(argv.prod, gulp.dest('.tmp/assets/javascript')))
    .pipe(when(argv.prod, when('*.js', gzip({append: true}))))
    .pipe(when(argv.prod, size({
      gzip: true,
      showFiles: true
    })))
    .pipe(gulp.dest('.tmp/assets/javascript'))
);

gulp.task('vendor-scripts', () => {
  const destDir = '.tmp/assets/javascript/vendor';
  return mergeStream(
    gulp.src('node_modules/jquery/dist/jquery.min.js').pipe(gulp.dest(destDir)),
    gulp.src('src/assets/javascript/vendor/*.js').pipe(gulp.dest(destDir))
  );
})

// 'gulp styles' -- creates a CSS file from your SASS, adds prefixes and
// creates a Sourcemap
// 'gulp styles --prod' -- creates a CSS file from your SASS, adds prefixes and
// then minwhenies, gzips and cache busts it. Does not create a Sourcemap
gulp.task('styles', () =>
  gulp.src(['src/assets/scss/*.scss', '!src/assets/scss/_*.scss'])
    .pipe(plumber({
      errorHandler: notify.onError('Error compilando SASS, revisa la consola\n <%= error.message %>'),
    }))
    .pipe(when(!argv.prod && !argv.cetelem, sourcemaps.init()))
    .pipe(sass({
      precision: 10,
      outputStyle: 'expanded',
    }))
    .pipe(postcss([
      autoprefixer({browsers: [
        'last 2 versions',
        'ie >= 9',
        'ChromeAndroid >= 2.3',
        'iOS >= 8',
        'Chrome >= 34',
        'Firefox >= 47',
        'Safari >= 6' ]})
    ]))
    .pipe(size({
      showFiles: true
    }))
    .pipe(when(argv.prod, rename({suffix: '.min'})))
    .pipe(when(argv.prod, when('*.css', cssnano({autoprefixer: false}))))
    .pipe(when(argv.prod, size({
      showFiles: true
    })))
    .pipe(when(argv.prod, rev()))
    .pipe(when(!argv.prod && !argv.cetelem, sourcemaps.write()))
    .pipe(when(argv.prod, gulp.dest('.tmp/assets/stylesheets')))
    .pipe(when(argv.prod, when('*.css', gzip({append: true}))))
    .pipe(when(argv.prod, size({
      gzip: true,
      showFiles: true
    })))
    .pipe(gulp.dest('.tmp/assets/stylesheets'))
    .pipe(when(!argv.prod, browserSync.stream()))
);

// Function to properly reload your browser
function reload(done) {
  browserSync.reload();
  done();
}
// 'gulp serve' -- open up your website in your browser and watch for changes
// in all your files and update them when needed
gulp.task('serve', (done) => {
  browserSync.init({
    // tunnel: true,
    open: false,
    notify: false,
    ghostMode: false,
    server: ['.tmp', 'dist']
  });
  done();

  // Watch various files for changes and do the needful
  gulp.watch(['src/**/*.md', 'src/**/*.html', 'src/**/*.yml', 'src/_includes/**/*'], gulp.series('build:site', reload));
  gulp.watch(['src/**/*.xml', 'src/**/*.txt'], gulp.series('site', reload));
  gulp.watch(['src/assets/javascript/**/*.js', '!src/assets/javascript/vendor/*.js'], gulp.series('scripts', reload));
  gulp.watch('src/assets/javascript/vendor/*.js', gulp.series('vendor-scripts', reload))
  gulp.watch('src/assets/scss/**/*.scss', gulp.series('styles'));
  gulp.watch('src/assets/images/**/*', gulp.series('images', reload));
});
