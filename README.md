# Requerimientos

## Del sistema
- Ruby 1.9.3 o superior
- NodeJS 5.4.1 o superior

## Dependencias de paquetes
* Dependencias de NodeJS
    * gulp: `npm install -g gulp-cli`
    * yarn: `npm install -g yarn`

* Dependencias de Ruby
    * bundler: `gem install bundler`

# Instalación
* Instala las dependencias de NodeJS: `yarn` o `npm install`
* Instala las dependencias de ruby: `bundle install`

# Comandos disponibles

# Ejecutar el proyecto para desarrollo
```sh
$ gulp [--prod]
```
`--prod` ejecutará el proyecto para desarrollo pero con las optimizaciones de producción.

# Compilación del proyecto
```sh
$ gulp build [--prod]
```
`--prod` compilará el proyecto con las optimizaciones para producción

# Configuración

- `/_config.yml`: Configuración de Jekyll
- `/_config.build.yml`: Configuración de Jekyll que sobreescribirá la de `_config.yml` cuando se lance el comando `gulp build`

# Siguientes pasos

## Cambia el nombre y descripción del proyecto
Edita el fichero `package.json` y cambia el valor de `name` y `description`.

## Generar favicons e iconos para dispositivos móviles
- Usar [Real Favicon Generator](https://realfavicongenerator.net/) para generar los iconos para los dispositivos.

## Habilitar indexación del site
- Elimina la `/` de `Disallow` en `robots.txt`
- Elimina el meta `robots` del fichero `src/_includes/head.html`.

## Habilitar información de SEO en las páginas
- Descomenta la gema `jekyll-seo-tag` de `_config.yml` y `Gemfile`
- Ejecuta en el raíz del proyecto: `bundle install`
- Añade `{% seo %}` en `head.html`
- Más información en: https://github.com/jekyll/jekyll-seo-tag

## Habilitar la generación de feeds
- Descomenta la gem `jekyll-feed` de `_config.yml` y `Gemfile`
- Ejecuta en el raíz del proyecto: `bundle install`
- Añade `{% feed_meta %}` en `head.html`
- Más información en: https://github.com/jekyll/jekyll-feed

## Habilitar la generación de sitemap
- Descomenta la gem `jekyll-sitemap` de `_config.yml` y `Gemfile`
- Ejecuta en el raíz del proyecto: `bundle install`
- Añade `{% feed_meta %}` en `head.html`
- Más información en: https://github.com/jekyll/jekyll-sitemap

# Estructura de directorios
```
├── Gemfile                   Dependencias de ruby
├── _config.prod.yml          Configuración para producción de Jekyll
├── _config.yml               Configuración base de Jekyll
├── gulp
│   └── tasks                 Tareas de Gulp
├── gulpfile.js               Fichero de carga de tareas de Gulp
├── material                  Material extra multimedia en formato fuente
├── package.json              Dependencias de NodeJS
├── src                       Fuentes del proyecto
│   ├── 404.html              Página 404 
│   ├── _includes             Includes de Jekyll
│   ├── _layouts              Layouts de Jekyll 
│   ├── assets                Assets a procesar fuera de Jekyll
│   │   ├── fonts
│   │   ├── images
│   │   │   └── proto         Imágenes que no sirven para producción, de prueba
│   │   ├── javascript        Javascript del proyecto
│   │   │   └── vendor        Librerías de terceros no gestionables por npm
│   │   └── scss              Fuentes SASS
│   ├── changelog.md          Página de control de cambios entre entregas
│   ├── index.html            Página de inicio
│   └── robots.txt

```
